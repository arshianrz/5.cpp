#ifndef RECTANGLE_H
#define RECTANGLE_H


class Rectangle
{
private:
    int Length_;  //Length
    int Width_;  //Width
public:
    Rectangle();
    void setLength(int);
    int getLength();
    void setWidth(int);
    int getWidth();
    void rectanglePrint(int,int);
    void run();

};

#endif // RECTANGLE_H
