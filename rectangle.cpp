#include "rectangle.h"
#include <iostream>

using namespace std;

Rectangle::Rectangle()
{
}

void Rectangle::setLength(int A)
{
    Length_=A;
}

int Rectangle::getLength()
{
    return Length_;
}

void Rectangle::setWidth(int B)
{
    Width_=B;
}

int Rectangle::getWidth()
{
    return Width_;
}

void Rectangle::rectanglePrint(int p, int q)
{
    for(int i=1 ; i<=p ; i++)
    {
        for(int j=1 ; j<=q ; j++)
        {
           if (i==1 || j==1 || i==p || j==q)
           {
               cout<<"*";
           }
           else
               cout<<" ";
        }
     cout<<endl;
    }
}

void Rectangle::run()
{
    int x,y;
    cout<<"Enter length.";
    cin>>x;
    cout<<"Enter width.";
    cin>>y;

    setLength(x);
    getLength();
    setWidth(y);
    getWidth();
    rectanglePrint(x,y);
}
